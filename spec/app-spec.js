var app = require("../app/app");

describe("app", function() {
    describe("#init", function() {
        it("should be defined", function() {
          expect(app.init).toBeDefined();
        });

        beforeEach(function(){
            var test = "<div id='content'></div>";
            document.body.insertAdjacentHTML(
                'afterbegin',
                test);
        });

        afterEach(function(){
            document.body.removeChild(document.getElementById("content"));
        })

        beforeEach(function(){
            app.init();
        });

        //Test 144 buttons created
        describe("gridInit", function(){
            it("should have 144 buttons",function(){
                var buttonCount = document.querySelectorAll('.btn').length;
                expect(buttonCount).toBe(144);
            });
        });

        //Test clicking number 2 highlights the correct amount of number buttons
        describe("numberButton", function(){
            it("should highlight multiples of number 54", function(){
                document.getElementById("number_2").click();
                var highlightCount = document.querySelectorAll(".btn--highlight").length;
                expect(highlightCount).toBe(72);
            });
        });

        //Test clicking previously clicked number removes highlights from multiples of number
        describe("numberButton", function(){
            it("should highlight multiples of number 54", function(){
                document.getElementById("number_2").click();
                var highlightCount = document.querySelectorAll(".btn--highlight").length;
                document.getElementById("number_2").click();
                highlightCount = document.querySelectorAll(".btn--highlight").length;
                expect(highlightCount).toBe(0);
            });
        });
    });
});
