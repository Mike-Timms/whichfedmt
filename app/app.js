// source goes here
var app = {
  // this is the entry point for your app.
  init: function() {
        //Creates number grid. Creates events listeners for each button.
        (function initGrid(){
            var grid = document.createElement("div");
            grid.classList.add("grid");

            //Store the current active number
            var activeNumber;

            for (a = 1; a <= 144; a++){
                var button = document.createElement("button");
                button.dataset.number = a;
                button.classList.add("btn");
                button.id = "number_" + a;
                button.innerHTML = a;
                button.addEventListener("click", function(){
                    var n = this.dataset.number;
                    if (n === activeNumber && activeNumber != "") {
                        resetGrid(activeNumber);
                        activeNumber = "";
                    }
                    else{
                        activeNumber = n;
                        setButtonMultiples(n);
                    }
                });
                //Append button to grid
                grid.appendChild(button);
            }
            //Append grid to content
            document.getElementById("content").appendChild(grid);
        })();

        //Work out multiples of selected number.
        function setButtonMultiples(number){
            //Loop through each button value
            for (b = 1; b <= 144; b++){
                //If remainder is 0, is a multiple, add highlight.
                if (b % number === 0){
                    document.getElementById("number_" + b).classList.add("btn--highlight");
                }
                //If not a multple,
                else{
                    removeButtonMultiples(b);
                }
            }
        }

        //Remove highlight if no. was a multple, but is no longer a multiple.
        function removeButtonMultiples(resetNumber){
            var targetButton = document.getElementById("number_" + resetNumber);
            if (targetButton.classList.contains("btn--highlight")){
                targetButton.classList.remove("btn--highlight");
            }
        }

        //Clear all highlights from all buttons.
        function resetGrid(activeNumber){
            //Loop through each button, remove btn--highlightc class
            for (c = 1; c <= 144; c++){
                document.getElementById("number_" + c).classList.remove("btn--highlight");
            }
            //Clear focus state from clicked button.
            document.getElementById("number_" + activeNumber).blur();
        }
    }
};

module.exports = app;
